from wtforms import Form, StringField, validators, ValidationError, IntegerField

class registration_form(Form):
    first_name = StringField('First Name', [validators.Length(min=4, max=25)])
    last_name = StringField('Last Name', [validators.Length(min=4, max=25)])
    phone_number = StringField('Phone Number', [validators.Length(min=4, max=25)])
    address = StringField('Address', [validators.Length(min=4)])
    pin = StringField('Pin', [validators.Length(min=6, max=6)])

    def validate_pin(form, field):
        if not field.data.isnumeric():
            raise ValidationError('input only number')

class login_form(Form):
    phone_number = StringField('Phone Number', [validators.Length(min=4, max=25)])
    pin = StringField('Pin', [validators.Length(min=6, max=6)])

class topup_form(Form):
    amount = IntegerField('amount', [validators.NumberRange(min=10000)])

    