import settings, uuid, datetime, hashlib, form as eForm, jwt
from flask import Flask, request
from flask_mongoengine import MongoEngine
from helper import respond_api, get_error_validation
from decorator import authorize
from db import User, Transaction

app =  Flask(__name__)
app.config['MONGODB_SETTINGS'] = settings.mongodb_setting
db = MongoEngine(app)

@app.route('/register/', methods=['POST'])
def register():
    try:
        form = eForm.registration_form(request.form)
        if form.validate():
            exist_user = User.objects(phone_number=form.phone_number.data).first()
            if exist_user:
                return respond_api(message_error="Phone Number already registered")
            user_id = uuid.uuid4()
            user = User()
            user.user_id = str(user_id)
            user.first_name = form.first_name.data
            user.last_name = form.last_name.data
            user.phone_number = form.phone_number.data
            user.address = form.address.data
            user.pin = hashlib.md5((form.pin.data).encode('utf-8')).hexdigest()
            user.save()
            return respond_api(result=user.json_register(), message="Register Success")
        else:
            return respond_api(message_error=get_error_validation(form))
    except Exception as e:
        app.logger.error(e)
        return respond_api(is_error=True)
    
@app.route('/login/', methods=['POST'])
def login():
    try:
        form = eForm.login_form(request.form)
        if form.validate():
            exist_user = User.objects(phone_number=form.phone_number.data, pin=hashlib.md5((form.pin.data).encode('utf-8')).hexdigest()).first()
            if not exist_user:
                return respond_api(message_error="Phone number and pin doesn’t match.")
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=30),
                'user_id': exist_user.user_id,
                'type': "access_token",
            }
            refresh_payload = payload.copy()
            refresh_payload['type'] = "refresh_token"
            access_token = jwt.encode(payload,settings.secret_key,algorithm='HS256')
            refresh_token = jwt.encode(refresh_payload,settings.secret_key,algorithm='HS256')
            result = {
                "access_token": access_token.decode("utf-8"),
                "refresh_token": refresh_token.decode("utf-8"),
            }
            return respond_api(result=result, message="Login Success")
            # return respond_api(message="Login Success")
        else:
            return respond_api(message_error=get_error_validation(form))
    except Exception as e:
        app.logger.error(e)
        return respond_api(is_error=True)

@app.route('/topup/', methods=['POST'])
@authorize
def topup():
    try:
        form = eForm.topup_form(request.form)
        if form.validate():
            topup_amount = form.amount.data
            transaction = Transaction()
            transaction.transaction_id = str(uuid.uuid4())
            transaction.amount = topup_amount
            transaction.user_id = request.user.user_id
            transaction.transaction_type = 0
            transaction.status = 1
            transaction.balance_before = request.user.balance
            request.user.balance += topup_amount
            transaction.balance_after = request.user.balance
            transaction.save()
            request.user.save()
            return respond_api(result=transaction.json_topup(), message="Topup Success")
        else:
            return respond_api(message_error=get_error_validation(form))
    except Exception as e:
        app.logger.error(e)
        return respond_api(is_error=True)

if __name__ == '__main__':
    app.run(debug=True)