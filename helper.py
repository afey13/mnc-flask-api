import settings
from flask import jsonify
def respond_api(is_error=False, result= {}, message=None, message_error=None):
    data = {
        "result": result,        
        "message": message,
        "status": True,
    }
    if is_error or message_error:
        data['message'] = "Error Server"
        if message_error:
            data['message'] = message_error
        data['status'] = False
    return jsonify(data)

def get_error_validation(form):
    for field_name, error_message in form.errors.items():
        return ("{} {}".format(field_name, error_message[0]))
        