import jwt, flask, settings
from functools import wraps
from helper import respond_api
from db import User

def authorize(f):
    @wraps(f)
    def decorated_function(*args, **kws):
            if not 'Authorization' in flask.request.headers:
               return respond_api(message_error="Unauthenticated")

            user = None
            data = flask.request.headers['Authorization']
            
            token = str.replace(str(data), 'Bearer ','')
            try:
                user = jwt.decode(token, settings.secret_key, algorithms=['HS256'])
                user = User.objects(user_id=user["user_id"]).first()
                if not user:
                    return respond_api(message_error="Unauthenticated")
                flask.request.user = user
            except:
                return respond_api(message_error="Unauthenticated")

            return f(*args, **kws)            
    return decorated_function