import datetime
import mongoengine as db

class User(db.Document):
    user_id = db.StringField()
    first_name = db.StringField()
    last_name = db.StringField()
    phone_number = db.StringField()
    address = db.StringField()
    pin = db.StringField()
    balance = db.FloatField(default=0)
    created_date = db.DateTimeField(default=datetime.datetime.utcnow())
    updated_date = db.DateTimeField(default=datetime.datetime.utcnow())

    def json_register(self):
        return {
            "user_id": self.user_id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "phone_number": self.phone_number,
            "address": self.address,
            "created_date": self.created_date.strftime("%Y-%m-%d"),
        }

class Transaction(db.Document):
    transaction_id = db.StringField()
    payment_id = db.StringField()
    user_id = db.StringField()
    amount = db.FloatField()
    balance_before = db.FloatField()
    balance_after = db.FloatField()
    transaction_type = db.IntField()
    status = db.IntField(default=0)
    remarks = db.StringField()
    created_date = db.DateTimeField(default=datetime.datetime.utcnow())
    updated_date = db.DateTimeField(default=datetime.datetime.utcnow())

    @staticmethod
    def list_status(val):
        status_list = {
            0: "PENDING",
            1: "SUCCESS",
            2: "CANCEL",
        }
        return transaction_type_list.get(val, "ERROR")

    @staticmethod
    def list_transaction_type(val):
        transaction_type_list = {
            1: "CREDIT",
            2: "DEBIT",
        }
        return transaction_type_list.get(val, "ERROR")


    def json_topup(self):
        return {
            "top_up_id": self.transaction_id,
            "amount_top_up": self.amount, 
            "balance_before": int(self.balance_before),
            "balance_after": int(self.balance_after),
            "created_date": self.created_date.strftime("%Y-%m-%d %H:%i:%s")
        }